import { EOL } from "os";

export function handleFile(filename: string, data: string): string {

  const linesin = data.split(EOL);
  var linesout = [];
  var i = 0;
  var remove = false;
  var last = false;

  for(let val of linesin) { // loop through lines
    if(val.substr(0,18) === "SINGLEMEMBERNUMBER") {
      i=0; // New component
    }
    else if((val.trim() === "PROCESSKEY:  4-040-4        Drilling") || (val.trim() === "PROCESSKEY:  4-040-2        Drilling")){
      remove = true;
    }
    else if(val.substr(0,12) === "PROCESSIDENT") {
      val = val.substr(0,14) + ++i;
      last = true;
    }
    if (!remove){
      linesout.push(val);
    }
    if (last) {
      remove = false;
      last = false;
    }
  }

  return [...linesout].join(EOL);

}
